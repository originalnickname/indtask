﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Photos9k
{
    public partial class fMain : Form
    {
        Image image = null;
        public fMain()
        {
            InitializeComponent();
            openPictureDialog.Filter = "Pictures|*.jpg;*.jpeg;*.png;*.gif|All files(*.*)|*.*";
            btn_rotateCCW.Cursor = Cursors.Hand;
            btn_rotateCW.Cursor = Cursors.Hand;
            btn_zoomIn.Cursor = Cursors.Hand;
            btn_zoomOut.Cursor = Cursors.Hand;
            btn_browse.Cursor = Cursors.Hand;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            if (openPictureDialog.ShowDialog() == DialogResult.Cancel) return;
            string filename = openPictureDialog.FileName;
            image = Image.FromFile(filename);
            txt_filePath.Text = filename;
            pb_picture.Image = image;
        }

        private void btn_rotateCCW_Click(object sender, EventArgs e)
        {
            if (image != null) image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            pb_picture.Image = image;
        }

        private void btn_rotateCW_Click(object sender, EventArgs e)
        {
            if (image != null) image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            pb_picture.Image = image;
        }

        private void btn_zoomOut_Click(object sender, EventArgs e)
        {
            if (image != null) {
                Size sz = new Size((int)(image.Width * 0.95), (int)(image.Height*0.95));
                image = (Image)(new Bitmap(image, sz));
                pb_picture.Image = image;
            }
        }

        private void btn_zoomIn_Click(object sender, EventArgs e)
        {
            if (image != null)
            {
                Size sz = new Size((int)(image.Width * 1.1), (int)(image.Height * 1.1));
                image = (Image)(new Bitmap(image, sz));
                pb_picture.Image = image;
            }
        }
    }
}
