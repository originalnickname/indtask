﻿namespace Photos9k
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.openPictureDialog = new System.Windows.Forms.OpenFileDialog();
            this.btn_rotateCCW = new System.Windows.Forms.Button();
            this.btn_rotateCW = new System.Windows.Forms.Button();
            this.btn_zoomIn = new System.Windows.Forms.Button();
            this.btn_zoomOut = new System.Windows.Forms.Button();
            this.txt_filePath = new System.Windows.Forms.TextBox();
            this.btn_browse = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pb_picture = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_picture)).BeginInit();
            this.SuspendLayout();
            // 
            // openPictureDialog
            // 
            this.openPictureDialog.FileName = "openFileDialog1";
            // 
            // btn_rotateCCW
            // 
            this.btn_rotateCCW.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_rotateCCW.BackgroundImage")));
            this.btn_rotateCCW.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_rotateCCW.Cursor = System.Windows.Forms.Cursors.No;
            this.btn_rotateCCW.Location = new System.Drawing.Point(334, 428);
            this.btn_rotateCCW.Name = "btn_rotateCCW";
            this.btn_rotateCCW.Size = new System.Drawing.Size(30, 30);
            this.btn_rotateCCW.TabIndex = 2;
            this.btn_rotateCCW.UseVisualStyleBackColor = true;
            this.btn_rotateCCW.Click += new System.EventHandler(this.btn_rotateCCW_Click);
            // 
            // btn_rotateCW
            // 
            this.btn_rotateCW.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_rotateCW.BackgroundImage")));
            this.btn_rotateCW.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_rotateCW.Cursor = System.Windows.Forms.Cursors.No;
            this.btn_rotateCW.Location = new System.Drawing.Point(370, 428);
            this.btn_rotateCW.Name = "btn_rotateCW";
            this.btn_rotateCW.Size = new System.Drawing.Size(30, 30);
            this.btn_rotateCW.TabIndex = 3;
            this.btn_rotateCW.UseVisualStyleBackColor = true;
            this.btn_rotateCW.Click += new System.EventHandler(this.btn_rotateCW_Click);
            // 
            // btn_zoomIn
            // 
            this.btn_zoomIn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_zoomIn.BackgroundImage")));
            this.btn_zoomIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_zoomIn.Cursor = System.Windows.Forms.Cursors.No;
            this.btn_zoomIn.Location = new System.Drawing.Point(442, 428);
            this.btn_zoomIn.Name = "btn_zoomIn";
            this.btn_zoomIn.Size = new System.Drawing.Size(30, 30);
            this.btn_zoomIn.TabIndex = 4;
            this.btn_zoomIn.UseVisualStyleBackColor = true;
            this.btn_zoomIn.Click += new System.EventHandler(this.btn_zoomIn_Click);
            // 
            // btn_zoomOut
            // 
            this.btn_zoomOut.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_zoomOut.BackgroundImage")));
            this.btn_zoomOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_zoomOut.Cursor = System.Windows.Forms.Cursors.No;
            this.btn_zoomOut.Location = new System.Drawing.Point(406, 428);
            this.btn_zoomOut.Name = "btn_zoomOut";
            this.btn_zoomOut.Size = new System.Drawing.Size(30, 30);
            this.btn_zoomOut.TabIndex = 5;
            this.btn_zoomOut.UseVisualStyleBackColor = true;
            this.btn_zoomOut.Click += new System.EventHandler(this.btn_zoomOut_Click);
            // 
            // txt_filePath
            // 
            this.txt_filePath.Location = new System.Drawing.Point(93, 462);
            this.txt_filePath.Name = "txt_filePath";
            this.txt_filePath.ReadOnly = true;
            this.txt_filePath.Size = new System.Drawing.Size(695, 20);
            this.txt_filePath.TabIndex = 6;
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(12, 460);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(75, 23);
            this.btn_browse.TabIndex = 7;
            this.btn_browse.Text = "Файл...";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pb_picture);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 410);
            this.panel1.TabIndex = 8;
            // 
            // pb_picture
            // 
            this.pb_picture.Location = new System.Drawing.Point(0, 0);
            this.pb_picture.Name = "pb_picture";
            this.pb_picture.Size = new System.Drawing.Size(782, 410);
            this.pb_picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb_picture.TabIndex = 0;
            this.pb_picture.TabStop = false;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 495);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_browse);
            this.Controls.Add(this.txt_filePath);
            this.Controls.Add(this.btn_zoomOut);
            this.Controls.Add(this.btn_zoomIn);
            this.Controls.Add(this.btn_rotateCW);
            this.Controls.Add(this.btn_rotateCCW);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fMain";
            this.Text = "Перегляд зображень";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_picture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openPictureDialog;
        private System.Windows.Forms.Button btn_rotateCCW;
        private System.Windows.Forms.Button btn_rotateCW;
        private System.Windows.Forms.Button btn_zoomIn;
        private System.Windows.Forms.Button btn_zoomOut;
        private System.Windows.Forms.TextBox txt_filePath;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pb_picture;
    }
}

